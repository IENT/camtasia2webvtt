var parseString = require('xml2js').parseString;
var sprintf = require('sprintf-js').sprintf;
document.getElementById('upload').addEventListener('change', readFileAsString)

var xml_object = {};
var toc;
var total_duration;

$("input:text").click(function() {
  $(this).parent().find("input:file").click();
});
$(".ui.icon.button").click(function() {
  $(this).parent().find("input:file").click();
});

$('input:file', '.ui.action.input')
  .on('change', function(e) {
    var name = e.target.files[0].name;
    $('input:text', $(e.target).parent()).val(name);
  });


function readFileAsString() {
  var files = this.files;
  if (files.length === 0) {
    console.log('No file is selected');
    return;
  }
  if (files[0].type !== 'text/xml') {
    console.log('The chosen file doesn\'t seem to be an XML file.')
    return;
  }
  var file_name = files[0].name.slice(0, -4);
  var reader = new FileReader();
  reader.onload = function(event) {
    parseString(event.target.result, function (err, result) {
      var output_file = 'WEBVTT\n\n';
      let custom_note = document.getElementById('note').value
      if (custom_note === "") {
        custom_note = "RWTH Aachen University"
      }
      output_file += `NOTE\n${custom_note}\n\n`

      xml_object = result;
      recurs2(xml_object['x:xmpmeta']);
      recurs(xml_object['x:xmpmeta']);
      toc = toc['xmpDM:markers'][0]['rdf:Seq'][0]['rdf:li'];
      for (const [index, toc_entry] of toc.entries()) {
        let chapter_name = toc_entry['rdf:Description'][0]['$']['xmpDM:name'];
        let start_time = toc_entry['rdf:Description'][0]['$']['xmpDM:startTime'];
        let stop_time;
        if (index !== toc.length-1) {
          stop_time = toc[index+1]['rdf:Description'][0]['$']['xmpDM:startTime'];
        } else { // last entry
          stop_time = total_duration;
        }
        output_file += `${index+1}\n${ms_to_timestamp(start_time)} --> ${ms_to_timestamp(stop_time)}\n${chapter_name}\n\n`
      }
      download(`${file_name}.vtt`, output_file);

    });
  };

  reader.readAsText(files[0]);
}

// recursive function used to find TableOfContents
function recurs(obj) {
  for (let prop in obj) {
    // on the attributes object '$', check if we have the wanted attribute
    if (prop === '$') {
      for (let attr in obj[prop]) {
        if (attr === 'xmpDM:trackType' && obj[prop][attr] === 'TableOfContents') {
          // we found the toc
          toc = obj;
        }
      }
    } else {
      if (Array.isArray(obj[prop])) {
        for (let subobj of obj[prop]) {
          recurs(subobj)
        }
      }
    }
  }
}
// recursive function used to find total time
function recurs2(obj) {
  for (let prop in obj) {
    // check if we have the prop xmpDM:duration
    if (prop === 'xmpDM:duration') {
      total_duration = obj[prop][0]['$']['xmpDM:value'];
    } else {
      if (Array.isArray(obj[prop])) {
        for (let subobj of obj[prop]) {
          recurs2(subobj)
        }
      }
    }
  }
}
// convert miliseconds count to timestamp
function ms_to_timestamp(ms) {
  ms = parseInt(ms);
  let total_seconds = ms/1000;
  let hours = parseInt(total_seconds / 3600)
  let minutes = parseInt(total_seconds / 60 - hours * 60)
  let seconds = total_seconds - hours * 3600 - minutes * 60
  return sprintf('%02d:%02d:%06.3f', hours, minutes, seconds);
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}
